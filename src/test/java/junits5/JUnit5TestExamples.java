package junits5;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class JUnit5TestExamples {

    @BeforeEach
    void initialiseEach() {
        System.out.println("Before each: initialisation");
    }

    @AfterEach
    void finaliseEach() {
        System.out.println("After each: finalisation");
    }

    @BeforeAll
    static void initialise() {
        System.out.println("Before all: initialisation");
    }

    @AfterAll
    static void finalise() {
        System.out.println("After all: finalisation");
    }

    @Test
    public void shouldRaiseAnException() {
        assertThrows(Exception.class, () -> {
            throw new Exception();
        });
    }

    @Test
    @Disabled
    public void disabledTest() {
        fail("Test must be ignored using JUnit 5 annotation");
    }

    @Test
    public void shouldFailBecauseTimeout() {
        assertTimeout(Duration.ofMillis(2), () -> Thread.sleep(1));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Hello", "World"})
    public void parameterizedTest(String word) {
        assertThat(asList("Hello", "World")).contains(word);
    }

    @DisplayName("Roman numeral")
    @ParameterizedTest(name = "\"{0}\" should be {1}")
    @CsvSource({"I, 1", "II, 2", "V, 5"})
    void withNiceName(String word, int number) {
    }

    @ParameterizedTest
    @CsvSource({
            "Hello, Loïc",
            "New, World"
    })
    public void parameterizedTest_multivalues(String word1, String word2) {
        assertTrue(("Hello".equals(word1) && "Loïc".equals(word2)
                || ("New".equals(word1) && "World".equals(word2))));
    }

    @DisplayName("Should calculate the correct sum")
    @ParameterizedTest(name = "{index} => a={0}, b={1}, sum={2}")
    @MethodSource("sumProvider")
    void sum(int a, int b, int sum) {
        assertEquals(sum, a + b);
    }

    private static Stream<Arguments> sumProvider() {
        return Stream.of(
                Arguments.of(1, 1, 2),
                Arguments.of(2, 3, 5)
        );
    }

}
